datalife_stock_unit_load_cases
==============================

The stock_unit_load_cases module of the Tryton application platform.

[![Build Status](http://drone.datalifeit.es:8050/api/badges/datalifeit/trytond-stock_unit_load_cases/status.svg)](http://drone.datalifeit.es:8050/datalifeit/trytond-stock_unit_load_cases)

Installing
----------

See INSTALL


License
-------

See LICENSE

Copyright
---------

See COPYRIGHT
